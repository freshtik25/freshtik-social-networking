import React from 'react';
import s from './Navbar.module.css'
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <nav className={`${s.nav} ${s.item}`}>
            <div>
                <NavLink to="/profile" activeClassName={s.activeLink}> Profile </NavLink>
            </div>
            <div className={s.active}>
                <NavLink to="/dialogs" activeClassName={s.activeLink}> Messages </NavLink>
            </div>
            <div>
                <NavLink to="/users" activeClassName={s.activeLink}> Users </NavLink>
            </div>
            <div>
                <NavLink to="/news" activeClassName={s.activeLink}> News </NavLink>
            </div>
            <div>
                <a> Music </a>
            </div>
        </nav>
    );
}

export default Navbar;