import React from "react";
import preloader from "../../../assets/images/preloader.svg";
import style from "./preloader.module.css";

let Preloader = (props) => {
    return <img className={style.preloader} src={preloader}/>
}

export default Preloader;