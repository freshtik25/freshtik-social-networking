import React from "react";
import styles from "./users.module.css";
import userPhoto from "../../assets/images/user.png";
import {NavLink} from "react-router-dom";

let Users = (props) => {

    let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);

    let pages = [];

    if (pagesCount > 10) {
        pages = []
        if (props.currentPage <= 4) {
            for (let i = 1; i <= props.currentPage + 2; i++) {
                pages.push(i)
            }
            pages.push("...", pagesCount)
        } else if (props.currentPage > 4 && props.currentPage < pagesCount - 4) {
            pages.push(1, "...")
            for (let i = props.currentPage - 2; i <= props.currentPage + 2; i++) {
                pages.push(i);
            }
            pages.push("...", pagesCount)
        } else {
            pages.push(1,"...")
            for (let i = props.currentPage - 2; i <= pagesCount; i++){
                pages.push(i)
            }
        }
    } else {
        for (let i=1; i <= pagesCount; i++) {
            pages.push(i)
        }
    }


    return (
        <div>
            <div>
                {pages.map(p => {
                    return <span className={props.currentPage === p && styles.selectedPage}
                                 onClick={(e) => {
                                     props.onPageChanged(p)
                                 }}> {p} </span>
                })}
            </div>
            {
                props.users.map(u =>
                    <div key={u.id}>
                        <span>
                            <div>
                                <NavLink to={'/profile/' + u.id}>
                                    <img src={u.photos.small != null ? u.photos.small : userPhoto}
                                        className={styles.userPhoto}/>
                                </NavLink>
                            </div>
                            <div>
                                {u.followed
                                    ? <button onClick={() => props.unfollow(u.id)}>Unfollow</button>
                                    : <button onClick={() => props.follow(u.id)}>Follow</button>
                                }
                            </div>
                        </span>
                        <span>
                            <span>
                                <div>{u.name}</div>
                                <div>{u.status}</div>
                            </span>
                            <span>
                                <div>{"u.location.country"}</div>
                                <div>{"u.location.city"}</div>
                            </span>
                        </span>
                    </div>
                )
            }
        </div>
    )
}

export default Users;