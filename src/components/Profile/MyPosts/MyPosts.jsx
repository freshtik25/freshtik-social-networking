import React from 'react';
import s from './MyPosts.module.css';
import Post from './Post/Post'

const MyPosts = (props) => {

    let postsElement = props.posts.map(post => <Post message={post.message} like_count={post.likesCount}/>);
    let newPostElement = React.createRef();

    let onPostChange = () => {
        let text = newPostElement.current.value;
        props.updateNewPostText(text);
    }

    let onAddPost = () => {
        props.addPost()
    }

    return (
        <div className={s.postsBlock}>
            <h3>My posts</h3>
            <div>
                <div>
                    <textarea onChange={onPostChange} ref={newPostElement}
                              value={props.newPostText} placeholder='Enter your post'> </textarea>
                </div>
                <div>
                    <button onClick={onAddPost}>Add post</button>
                </div>
                <br/>
            </div>

            <div>
                {postsElement}
            </div>
        </div>
    );
}

export default MyPosts;