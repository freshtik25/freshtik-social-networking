const SEND_MESSAGE = 'SEND-MESSAGE';
const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';

let initialState = {
    dialogs: [
        {id: 1, name: "Andrew"},
        {id: 2, name: "Dmitry"},
        {id: 3, name: "Sasha"},
        {id: 4, name: "Sveta"},
        {id: 5, name: "Valera"},
        {id: 6, name: "Viktor"},
    ],
    messages: [
        {id: 1, message: "Hi"},
        {id: 2, message: "What's is your it-kam?"},
        {id: 3, message: "Yo"},
        {id: 4, message: "YoYo"},
        {id: 5, message: "Yee"},
    ],
    newMessageBody: ''
}

const dialogsReducer = (state = initialState, action) => {

    let stateCopy;

    switch (action.type) {
        case SEND_MESSAGE:
            let body = state.newMessageBody;
            stateCopy = {
                ...state,
                newMessageBody: '',
                messages: [...state.messages, {id: 7, message: body}]
            }
            return stateCopy;
        case UPDATE_NEW_MESSAGE_BODY:
            return {
                ...state,
                newMessageBody: action.body
            };
        default:
            return state;
    }
}
export const sendMessage = () => ({type: SEND_MESSAGE});
export const updateNewMessageBody = (body) => {
    return {type: UPDATE_NEW_MESSAGE_BODY, body: body};
}

export default dialogsReducer;