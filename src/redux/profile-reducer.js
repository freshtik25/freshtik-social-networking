const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
const SET_USER_PROFILE = 'SET_USER_PROFILE';

let initialState = {
    posts: [
        {id: 1, message: "Hi. how are you?", likesCount: 0},
        {id: 2, message: "I'm fine", likesCount: 15},
        {id: 3, message: "Wow!", likesCount: 45},
        {id: 4, message: "It's fire", likesCount: 5},
        {id: 5, message: "ooo", likesCount: 17},
    ],
    newPostText: '',
    profile: null,
}

export const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST : {
            let newPost = {
                id: 6,
                message: state.newPostText,
                likesCount: 9
            };
            return {
                ...state,
                posts: [...state.posts, newPost],
                newPostText: ''
            };
        }
        case SET_USER_PROFILE: {
            return {...state, profile: action.profile}
        }

        case UPDATE_NEW_POST_TEXT: {
            return {
                ...state,
                newPostText: action.newText
            }
        }
        default:
            return state;
    }
}
export const addPost = () => ({type: ADD_POST});
export const setUserProfile = (profile) => ({type: SET_USER_PROFILE, profile})
export const updateNewPostText = (text) => {
    return {type: UPDATE_NEW_POST_TEXT, newText: text};
}

export default profileReducer;