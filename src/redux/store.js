import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";

let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 1, message: "Hi. how are you?", likesCount: 0},
                {id: 2, message: "I'm fine", likesCount: 15},
                {id: 3, message: "Wow!", likesCount: 45},
                {id: 4, message: "It's fire", likesCount: 5},
                {id: 5, message: "ooo", likesCount: 17},
            ],
            newPostText: ''
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: "Andrew"},
                {id: 2, name: "Dmitry"},
                {id: 3, name: "Sasha"},
                {id: 4, name: "Sveta"},
                {id: 5, name: "Valera"},
                {id: 6, name: "Viktor"},
            ],
            messages: [
                {id: 1, message: "Hi"},
                {id: 2, message: "What's is your it-kam?"},
                {id: 3, message: "Yo"},
                {id: 4, message: "YoYo"},
                {id: 5, message: "Yee"},
            ],
            newMessageBody: ''
        },
        sidebar: {}
    },
    _callSubscriber() {
        console.log("State is change")
    },

    getState() {
        return this._state
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {

        this._state.profilePage = profileReducer(this._state.profilePage, action)
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action)
        this._state.sidebar = sidebarReducer(this._state.sidebar, action)

        this._callSubscriber(this._state);

    }
}

export default store;
window.state = store
